---
title: 
subtitle: 
comments: false
---

<div class="row">
    <div class="col-lg-10 col-md-10"><h1>Android App Development</h1></div>
    <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <img src="/gallery/android/Android_Robot_200.png" style="width:64px;" alt="Icon">
    </div>
</div>

Whilst learning Java in 2010, I was interested in applying my skills by creating some simple Live Wallpaper applications for Android OS. Little did I know they would go on to accumulate over 5 million downloads with a 4.3 average rating! I think I got lucky finding a niche in the market before the Google Play Store became oversaturated with competition, but nonetheless I am proud of what I achieved and like to think I maintained a high level of quality whilst looking after my user base. The income generated from these apps allowed me to focus on my masters degree without having to find a part time job.

Unfortunately, after selling the source code and rights to the apps to help fund my wedding in 2014, the new owner hasn't done much to maintain or update them, instead opting to maximize profits with advertisements whilst removing some of the free versions altogether.

Anyway, here are the apps...

<div class="well" style="z-index:1;">
<canvas class="snow hidden-sm hidden-xs"></canvas>

<div class="row">
    <div class="col-12">
        <h2 class="opacity text-center">Snowflakes Live Wallpaper</h2>
    </div>
</div>

Two Live Wallpapers roughly translated into 13 different languages, with a free version linking to a paid version via it's limited settings menu. Swiping left and right acts as a gust of wind, blowing the snowflakes sideways. The full version is highly customizable - users can change the speed, shapes, size, colors of the snowflakes and backgrounds, choose from a set of colorizable background textures or use a personal background image, save and load presets, change the FPS, and more. Why wouldn't you want to upgrade, right?

By clicking on the banner images below, you can see the apps as they appeared in the Play Store during my time of ownership in 2013 (thanks to the awesome WayBack Machine):

<div class="row">
    <div class="col-lg-6 col-md-6">
        <a href="https://web.archive.org/web/20130429121427/https://play.google.com/store/apps/details?id=chris.cooper.snowflakes" target="_blank"><img class="cc-fade" src="/gallery/android/snowflakes/feature%20graphic%20free.jpg" alt="Banner Free"></a>
    </div>
    <div class="col-lg-6 col-md-6">
        <a href="https://web.archive.org/web/20130312102523/https://play.google.com/store/apps/details?id=chris.cooper.snowflakes.full" target="_blank"><img class="cc-fade" src="/gallery/android/snowflakes/feature%20graphic.jpg" alt="Banner Full"></a>
    </div>
</div>

---

<div class="opacity">

<h3>What did I learn?</h3>
<ul>
    <li>Android SDK debugging and development.</li>
    <li>Design, Development, Art (sort of), QA, Strategy, Marketing, PR - I had to do it all and enjoyed every minute of it!</li>
    <li>Maintenance challenges (backwards compatibility, fragmented userbase).</li>
    <li>Combating piracy was a constant battle, not just people downloading illegally but also rogue developers encroaching on my IP.</li>
</ul>
</div>

---

<h3 class="opacity">Demo Video</h3>

<div class="">
    <div class="row">
<iframe class="col-lg-12" width="100%" height="533" src="https://www.youtube-nocookie.com/embed/LY2IIvcIIJQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
</div>
</div>

<p>
Most of the other apps I released were modifications of the Snowflakes Live Wallpaper idea. Some were real flops whilst others went on to achieve more downloads and higher ratings (but less income). I consider them experiments in Marketing more than anything else, as the code and options are fundamentally the same throughout, just with different shapes, presets and physics.  Unfortunately, the new owner has since removed all the free versions from the Play Store. Here are some notable mentions:
</p>


<!-- Other Apps (desktop) -->
<div class="hidden-sm hidden-xs">

<div class="well">
<div class="row">
    <div class="col-lg-12">
        <h3>Androids! Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div style="width:14em;float:left;padding-right:14px;">
            <a href="https://web.archive.org/web/20130127023323/https://play.google.com/store/apps/details?id=chris.cooper.androids.pro" target="_blank"><img class="cc-fade" src="/gallery/android/androids/feature%20graphic.jpg" alt="Banner Free"></a>
        </div>
        <div class="justify">
            A set of Live Wallpapers in a space / sci-fi theme. The free version had plenty of features and was supported by a small AdMob banner at the top of the settings menu. Users were occasionally reminded that they could 'Go Pro!' to remove the advert, gain special 'Monster' shapes, and save their own settings. It was quite a successful strategy, and ended up gaining the highest user ratings and most free downloads (3+ million) of all my apps, even the YouTube demo has generated 900k+ views!
        </div>
    </div>
</div>

---

<div class="row">
    <div class="col-lg-12">
        <h3>Leaves Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div style="width:14em;float:left;padding-right:14px;">
            <a href="https://web.archive.org/web/20130703180319/https://play.google.com/store/apps/details?id=chris.cooper.leaves.full" target="_blank"><img class="cc-fade" src="/gallery/android/leaves/feature%20graphic.jpg" alt="Banner Free"></a>
        </div>
        <div class="justify">
            A set of Live Wallpapers with an Autumnal feel, with a restrictive free version linking to a feature rich paid version. This set didn't do so well, probably about 50k free downloads in total. Maybe I got unlucky, maybe there wasn't enough demand, maybe I messed up the marketing! Who knows? It never really took off like Androids or Snowflakes. It's a shame as this one had my favourite color scheme and physics, which swayed gently down the screen as if falling from a tree.
        </div>
    </div>
</div>

---

<div class="row">
    <div class="col-lg-12">
        <h3>Hearts Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div style="width:14em;float:left;padding-right:14px;">
            <a href="https://web.archive.org/web/20130703184412/https://play.google.com/store/apps/details?id=chris.cooper.hearts.full" target="_blank"><img class="cc-fade" src="/gallery/android/hearts/feature%20graphic.jpg" alt="Banner Free"></a>
        </div>
        <div class="justify">
        A set of Live Wallpapers with a restricted free version linking to a feature rich full version. The obvious theme here is love, hearts, cupid, etc. This set didn't do so well either, only achieving around 100k free downloads.
        </div>
    </div>
</div>
</div>

Apart from the above Live Wallpapers, I also created a widget for the home screen called 'Click Clock widget'. This was a bit more complicated than my previous efforts, utilizing web services, geolocation features, and hardware info. I thought it was a neat idea with a unique selling point, but it didn't do as well as I'd hoped (only 2k downloads). I can't be sure why but I guess got lost in the sea of other homescreen clock widgets available on the market at the time...

<!-- Click Clock widget -->
<div class="well">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">Click Clock Widget</h2>
        </div>
    </div>

The unique selling point is that no instructions or settings are needed - you just click on various parts of the widget itself in order to cycle through the options. You can change the font, date format, background, or toggle between battery level/current weather. I discovered that obtaining correct weather information based on location was quite tricky as showing a moon or sun to the user depends on sunset and sunrise times around the globe. So obviously I had to utilize GPS and/or WiFi geolocation functionality, as well as plugging into a weather forecasting web service. Wanting to cut costs, I opted to decipher Google's non-public weather API. Despite working well at the time, this was not such a good idea as the API contract changed several times, thus breaking the app. It was also non-trivial trying to keep the clock running, as Android has a habit of deciding when it should terminate running programs and services, including the app's service worker which updates the time.

I think I took the idea a bit far really. Whilst it's neat to be able to customize via the widget itself, it wasn't really very practical. I would like to have added a settings area, perhaps via a small icon on the widget itself. The widget would flip around to reveal some options: toggle locked/editable, 12/24 hour format, help page. Some options were a bit pointless too, and could have been autodetected based on location.

<div class="row" width="100%">
    <h3 class="card-title text-center">Screenshots</h3>
    <img class="col-lg-4 col-md-4" src="/gallery/android/clickclock/01.jpg" alt="Screenshot 02">
    <img class="col-lg-4 col-md-4" src="/gallery/android/clickclock/usage_sm.jpg" alt="Screenshot 01">
    <img class="col-lg-4 col-md-4" src="/gallery/android/clickclock/05.jpg" alt="Screenshot 03">
</div>

---

<div>
    <h3>What did I learn?</h3>
    <ul>
        <li>Android's GUI layout system to display content as an Android Widget.</li>
        <li>Access hardware features, such as battery info and geolocation features using GPS and/or WiFi.</li>
        <li>Never use a secret API that isn't intended for public use!</li>
    </ul>
</div>
</div>
</div>

<!-- Other Apps (mobile) -->
<div class="hidden-md hidden-lg">

<div class="well">
<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Androids! Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 text-center">
        <div>
            <a href="https://web.archive.org/web/20130127023323/https://play.google.com/store/apps/details?id=chris.cooper.androids.pro" target="_blank"><img src="/gallery/android/androids/feature%20graphic.jpg" alt="Banner Free"></a>
            <h5>(Click image for Play Store listing)</h5>
        </div>
        <div class="justify">
            A set of Live Wallpapers in a space / sci-fi theme. The free version had plenty of features and was supported by a small AdMob banner at the top of the settings menu. Users were occasionally reminded that they could 'Go Pro!' to remove the advert, gain special 'Monster' shapes, and save their own settings. It was quite a successful strategy, and ended up gaining the highest user ratings and most free downloads (3+ million) of all my apps, even the YouTube demo has generated 900k+ views!
        </div>
    </div>
</div>

---

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Leaves Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 text-center">
        <div>
            <a href="https://web.archive.org/web/20130703180319/https://play.google.com/store/apps/details?id=chris.cooper.leaves.full" target="_blank"><img src="/gallery/android/leaves/feature%20graphic.jpg" alt="Banner Free"></a>
            <h5>(Click image for Play Store listing)</h5>
        </div>
        <div class="justify">
            A set of Live Wallpapers with an Autumnal feel, with a restrictive free version linking to a feature rich paid version. This set didn't do so well, probably about 50k free downloads in total. Maybe I got unlucky, maybe there wasn't enough demand, maybe I messed up the marketing! Who knows? It never really took off like Androids or Snowflakes. It's a shame as this one had my favourite color scheme and physics, which swayed gently down the screen as if falling from a tree.
        </div>
    </div>
</div>

---

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Hearts Live Wallpaper</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 text-center">
        <div>
            <a href="https://web.archive.org/web/20130703184412/https://play.google.com/store/apps/details?id=chris.cooper.hearts.full" target="_blank"><img src="/gallery/android/hearts/feature%20graphic.jpg" alt="Banner Free"></a>
            <h5>(Click image for Play Store listing)</h5>
        </div>
        <div class="justify">
        A set of Live Wallpapers with a restricted free version linking to a feature rich full version. The obvious theme here is love, hearts, cupid, etc. This set didn't do so well either, only achieving around 100k free downloads.
        </div>
    </div>
</div>
</div>

Apart from the above Live Wallpapers, I also created a widget for the home screen called 'Click Clock widget'. This was a bit more complicated than my previous efforts, utilizing web services, geolocation features, and hardware info. I thought it was a neat idea with a unique selling point, but it didn't do as well as I'd hoped (only 2k downloads). I can't be sure why but I guess got lost in the sea of other homescreen clock widgets available on the market at the time...

<!-- Click Clock widget -->
<div class="well">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">Click Clock Widget</h2>
        </div>
    </div>

The unique selling point is that no instructions or settings are needed - you just click on various parts of the widget itself in order to cycle through the options. You can change the font, date format, background, or toggle between battery level/current weather. I discovered that obtaining correct weather information based on location was quite tricky as showing a moon or sun to the user depends on sunset and sunrise times around the globe. So obviously I had to utilize GPS and/or WiFi geolocation functionality, as well as plugging into a weather forecasting web service. Wanting to cut costs, I opted to decipher Google's non-public weather API. Despite working well at the time, this was not such a good idea as the API contract changed several times, thus breaking the app. It was also non-trivial trying to keep the clock running, as Android has a habit of deciding when it should terminate running programs and services, including the app's service worker which updates the time.

I think I took the idea a bit far really. Whilst it's neat to be able to customize via the widget itself, it wasn't really very practical. I would like to have added a settings area, perhaps via a small icon on the widget itself. The widget would flip around to reveal some options: toggle locked/editable, 12/24 hour format, help page. Some options were a bit pointless too, and could have been autodetected based on location.

<div class="row">
    <h3 class="card-title text-center">Screenshots</h3>
    <div class="row text-center">
        <div class="main-carousel" style="height:256px;" data-flickity='{ "autoPlay": true, "cellAlign": "center", "wrapAround": true, "lazyLoad": true, "lazyLoad": 1, "pageDots": true, "prevNextButtons": false}'>
            <div style="height:256px;" class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/android/clickclock/usage_sm.jpg"/></div>
            <div style="height:256px;" class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/android/clickclock/01.jpg"/></div>
            <div style="height:256px;" class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/android/clickclock/02.jpg"/></div>
            <div style="height:256px;" class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/android/clickclock/03.jpg"/></div>
            <div style="height:256px;" class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/android/clickclock/05.jpg"/></div>
        </div>
    </div>
</div>

---

<div>
    <h3>What did I learn?</h3>
    <ul>
        <li>Android's GUI layout system to display content as an Android Widget.</li>
        <li>Access hardware features, such as battery info and geolocation features using GPS and/or WiFi.</li>
        <li>Never use a secret API that isn't intended for public use!</li>
    </ul>
</div>
</div>

</div>

---

## Conclusion

I learned a lot from my experiments with Android, it was a great way to learn Java whilst having some fun. I don't think I could recreate the early success I had today as the market has changed so much - it's virtually impossible to get noticed in the Play Store without a vast amount of luck as there are big businesses dominating the rankings with hefty marketing budgets. I believe the key for me was to get in there early with a simple, quality product.

<hr class="small"/>

<script>
    var canvas = document.querySelector('.snow'),
    ctx = canvas.getContext('2d'),
    windowW = window.innerWidth,
    windowH = window.innerHeight,
    numFlakes = 200,
    flakes = [];

function Flake(x, y) {
  var maxWeight = 5,
      maxSpeed = 3;
  
  this.x = x;
  this.y = y;
  this.r = randomBetween(0, 1);
  this.a = randomBetween(0, Math.PI);
  this.aStep = 0.01;

  
  this.weight = randomBetween(2, maxWeight);
  this.alpha = (this.weight / maxWeight);
  this.speed = (this.weight / maxWeight) * maxSpeed;
  
  this.update = function() {
    this.x += Math.cos(this.a) * this.r;
    this.a += this.aStep;
    
    this.y += this.speed;
  }
  
}

function init() {
  var i = numFlakes,
      flake,
      x,
      y;
  
  while (i--) {
    x = randomBetween(0, windowW, true);
    y = randomBetween(0, windowH, true);
    
    
    flake = new Flake(x, y);
    flakes.push(flake);
  }
  
  scaleCanvas();
  loop();  
}

function scaleCanvas() {
  canvas.width = windowW;
  canvas.height = windowH;
}

function loop() {
  var i = flakes.length,
      z,
      dist,
      flakeA,
      flakeB;
  
  // clear canvas
  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, windowW, windowH);
  ctx.restore();
  
  // loop of hell
  while (i--) {
    
    flakeA = flakes[i];
    flakeA.update();
    
    ctx.beginPath();
    ctx.arc(flakeA.x, flakeA.y, flakeA.weight, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'rgba(255, 255, 255, ' + flakeA.alpha + ')';
    ctx.fill();
    
    if (flakeA.y >= windowH) {
      flakeA.y = -flakeA.weight;
    }  
  }
  
  requestAnimationFrame(loop);
}

function randomBetween(min, max, round) {
  var num = Math.random() * (max - min + 1) + min;

  if (round) {
    return Math.floor(num);
  } else {
    return num;
  }
}

function distanceBetween(vector1, vector2) {
  var dx = vector2.x - vector1.x,
      dy = vector2.y - vector1.y;

  return Math.sqrt(dx*dx + dy*dy);
}

init();
</script>