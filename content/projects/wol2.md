---
title: Wiley Online Library 2.0
subtitle: Spring-based OSGI development with dotCMS
comments: false

---
The challenge was to produce a modern, re-skinnable, journal delivery platform. Our clients could migrate from our older Wiley Online Library offering to take advantage of a responsive design, more user friendly experience, and customizable content.

<!---->

The project generated over $100m in revenue for Wiley & Sons Ltd, which helped in their purchase of Atypon - and hence the ultimate demise of the project. These notes aim to capture the essence of the beast that once was! By the end of the project, 25 websites had been produced.

<div class="row well hidden-sm hidden-xs">
    <h4 class="card-title text-center">Example sites</h4>
    <div class="main-carousel" data-flickity='{ "autoPlay": true, "cellAlign": "left", "contain": true, "wrapAround": true, "lazyLoad": true, "lazyLoad": 1, "pageDots": false}'>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/aaa.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/accp.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/ascpt.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/bes.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/esa.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/ila.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/lms.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/nph.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/physoc.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/rmets.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/zsl.jpg"/></div>
    </div>
</div>
<div class="row well hidden-md hidden-lg">
    <h4 class="card-title text-center">Example sites</h4>
    <div class="main-carousel main-carousel-dots" data-flickity='{ "autoPlay": true, "cellAlign": "left", "contain": true, "wrapAround": true, "lazyLoad": true, "lazyLoad": 1, "pageDots": true, "prevNextButtons": false}'>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/aaa.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/accp.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/ascpt.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/bes.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/esa.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/ila.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/lms.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/nph.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/physoc.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/rmets.jpg"/></div>
        <div class="carousel-cell"><img class="carousel-cell-image" data-flickity-lazyload="/gallery/wol2/zsl.jpg"/></div>
    </div>
</div>

<hr/>

<h3>What did I learn?</h3>

Some notable personal achievements were: 

* To collaborate with our data services team and implement the search API, based on UX requirements.
* Integrate Avectra (a third party user authorization system) for a major publisher.
* Scrape and import thousands of inconsistently formatted paragraphs (Editor Highlights) to maintain publisher's commentary on content, between Wiley Online Library versions.
* Configured SonarQube and Jenkins to build and evaluate projects to maintain code quality and provide feedback locally for developers.
* Working in a major corporation (over 5k employees), on a daily basis with teams ditributed across the globe (USA, UK, India, Russia, Germany).
* Participating in, and occasionally running the daily production monitoring meeting with key stakeholders from around the company.

<p>
    I helped develop the product using the following technologies:
</p>

<div class="container">
    <div class="row">
        <div class="col-md-2"><strong>Development:</strong></div>
        <div class="col-md-10">
            <span class="label label-primary">Java</span>
            <span class="label label-primary">Spring</span>
            <span class="label label-primary">JUnit</span>
            <span class="label label-primary">Velocity</span>
            <span class="label label-primary">ElasticSearch</span>
            <span class="label label-primary">MySQL</span>
            <span class="label label-primary">XML</span>
            <span class="label label-primary">dotCMS</span>
        </div>
        <hr/>
    </div>
    <div class="row">
        <div class="col-md-2"><strong>DevOps:</strong></div>
        <div class="col-md-10 align-right">
            <span class="label label-primary">Ansible</span>
            <span class="label label-primary">Vagrant</span>
            <span class="label label-primary">Git</span>
            <span class="label label-primary">Gradle</span>
            <span class="label label-primary">SonarQube</span>
            <span class="label label-primary">Jenkins</span>
        </div>
        <hr/>
    </div>
    <div class="row">
        <div class="col-md-2"><strong>Monitoring:</strong></div>
        <div class="col-md-10">
            <span class="label label-primary">Dynatrace</span>
            <span class="label label-primary">Pingdom</span>
            <span class="label label-primary">Kibana</span>
        </div>
        <hr/>
    </div>
</div>

---
### Our Team
<p>
Our team's responsibility was to deliver content between our datacentres and our frontend team, via dotCMS. We had to design systems based on incoming requirements, and collaborate with other teams to deliver production-ready code. 
</p>

<p>
Working in an agile environment, on a global scale. Daily standups, technical analysis and task estimation, specification workshops, monthly restrospectives, iteration planning. I worked directly with both the frontend team and the dataservices team, to discuss requirements and solutions. I also raised any concerns with the UX team / Business Analysts, if requirements weren't obvious or missed particular scenarios. We used JIRA to tracked our work, and Slack enabled us to communicate remotely in real time, if needs be.
</p>

<p>
Wiley were good to us, we were allowed to take time to learn interesting new technologies in our many mini-hackathons. If it wasn't for the Atypon aquisition, and subsequent redundancies, I would have happily continued my career there.
</p>

<hr/>
<h3>Development</h3>
<p>
We developed locally in Windows 7 using a VM generated from our Ansible scripts. In Windows, we used the Eclipse IDE, Java 7, PuTTY with Pageant to SSH into our various environments. We used Gradle for building. We used Apiary both to mock up services and for our unit test suites.
</p>
<p>
Most of the code backend we developed was Java / Spring OSGI plugins which we used to extend the functionality of dotCMS. dotCMS provided a convenient way for our clients to be able to publish and edit their own content online.
</p>
<p>
Our workflow was fairly standard - develop locally in a VM, push changes to our git repos, which would automatically build and deploy the latest code and assets to our dev environment. Possibly do further development directly on the dev environment (for dotCMS push publishing reasons), then off to QA enviroment for functional testing. If all is good, on to staging for regression testing against real data and services, and then lastly to our performance testing environment before being approved for release. We would release every 3 months or so, with extra patch releases whenever necessary. Typically we would use Jenkins to build, deploy and publish to each environment, although some manual work was occasionally needed.
</p>
<p>
Towards the end of the project, the business was less keen to develop new features and so focus shifted towards increasing stability, improving documentation, and ironing out defects.
</p>

---
<h3>Prototype Webpages</h3>
<p>
Each website had 4 different types of webpages - a home page, a journal home page, search results page, and a blank page for any additional needs.
</p>
<p>
The idea being that each type of page could largley be populated based on convention - i.e. site-specific configuration and content authored in dotCMS. This approach made it easy and fast to produce new websites, albiet at the expense of a slight reduction in flexibility.
</p>
<div class="row well">
    <div class="col-md-12 text-center">
        <h4>Journal Home Page</h4>
        <image class="gif" src="/gifs/wol2_journal_home_page.gif" alt="[Journal Home Page animated GIF]"></image>
        <small>Here you can see one of the prototype pages, full of reusable / configurable widgets <br/>(Current Issue, Journal Highlights, Browser Articles, Resources, Etc.).</small>
    </div>
</div>

<p>
Perhaps one of the most significant OSGI plugins we developed was an AJAX friendly search results page.  This allows users to search for articles within a publisher's catalogue of journals:
</p>

<div class="row well">
    <div class="col-md-12 text-center">
        <h5>Search Results Page</h5>
        <image class="gif" src="/gifs/wol2_simple_search.gif" alt="[Simple Search animated GIF]"></image>
        <small>Search for articles by search term, journal, subject / taxonomy, and date range.<br/>Users could filter, sort, and paginate through results.</small>
    </div>
</div>

<p>
Probably one of the most challenging aspects of making the search work, was the UX requirements was had to follow. Although you can't see it here, the URL/s and subject taxonomies had been dictated by our clients and UX designers. Some of the requirments were impossible and some made our life downright difficult, but we managed to find a compromise by introducing some undesirable character conversions at various stages throughout the request / response cycle. Ultimately, our clients were happy and we ended up with more user-friendly URLs. 
</p>

## Conclusion

<p>
I enjoyed working with the team at Wiley. WOL2 was an interesting project to work on and the company encouraged innovation with annual hackathons and knowledge sharing activities. It gave me experience working with CMS's, which in turn has peaked my interest in serverless architectures, and I feel like I understand a lot more of the devops side of things now.
</p>

<!-- -->

<hr class="small"/>