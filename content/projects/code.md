---
title: 
subtitle: 
comments: false

---

<div class="row">
    <div class="col-lg-9 col-md-9"><h1>Code Repositories</h1></div>
</div>

One day I will get around to tidying up some of these repos for good and where possible, release them into the public domain. 

In the meantime, if you'd like to see more examples of my work or take a look at some code (personal, educational, and freelance projects), please get in touch via the contact form and I can share some GitLab access credentials with you.

Although each project's documentation should provide a good overview, I'd be more than happy to help answer any questions you may have. 

<hr/>

<hr class="small"/>