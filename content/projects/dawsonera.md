---
title: 
subtitle: 
comments: false

---

<div class="row">
    <div class="col-lg-9 col-md-9"><h1>DawsonEra</h1></div>
    <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
        <a href="https://www.dawsonera.com/about" target="_blank"><img src="/gallery/dawsonera/logo.png" alt="Icon"></a>
    </div>
</div>

<h2>Full Stack development</h2>


DawsonERA is a best of breed eBook platform delivering over 2 million pageviews a month and in excess of 2,000 unique book downloads per month. More than 270k front list titles from more than 500 leading international publishers are available to purchase or rent in real-time on the platform.

As a part of Betrams Books' continued relationship with Semantico Ltd (now Highwire Press), this £2 million project was essentially to modernize an existing product with a responsive design, personalisation, and several other upgrades.

Picked up a dated product and in a small team of 3 engineers, 1 front end designer and 1 project manager, worked together to implement new functionality, and upgrade outdated systems. Here is what we achieved...

<hr/>

<h3>What did I learn?</h3>

Some notable personal achievements were: 

* Servebase (payment processing)
* Rebuilt frontend for responsive web (didn't do much CSS, just Freemarker and a bit of AJAX)
* Customization (My Bookshelf, Commenting, My Favourites, Recently Viewed)
* Social Media (addThis plugin)
* Internationalized all text
* Solr plugin to cache institutional access details
* Migrated from COUNTER 3 to COUNTER 4
* Simulation to fix incorrect usage stats
* Improved search functionality (advanced search, faceting, saved searches, email notications, infinite scrolling)

<p>
    I helped develop the product using the following technologies:
</p>

<div class="container">
    <div class="row">
        <div class="col-md-2"><strong>Back End:</strong></div>
        <div class="col-md-10">
            <span class="label label-primary">Java</span>
            <span class="label label-primary">Spring</span>
            <span class="label label-primary">TestNG</span>
            <span class="label label-primary">Hibernate</span>
            <span class="label label-primary">PostgreSQL</span>
            <span class="label label-primary">Solr</span>
            <span class="label label-primary">Tomcat</span>
        </div>
        <hr/>
    </div>
    <div class="row">
        <div class="col-md-2"><strong>Front End:</strong></div>
        <div class="col-md-10">
            <span class="label label-primary">HTML</span>
            <span class="label label-primary">Freemarker</span>
            <span class="label label-primary">JQuery</span>
        </div>
        <hr/>
    </div>
    <div class="row">
        <div class="col-md-2"><strong>DevOps:</strong></div>
        <div class="col-md-10 align-right">
            <span class="label label-primary">GitLab</span>
            <span class="label label-primary">Linux</span>
            <span class="label label-primary">Puppet</span>
            <span class="label label-primary">Jenkins</span>
            <span class="label label-primary">Maven</span>
        </div>
        <hr/>
    </div>
</div>




<hr class="small"/>