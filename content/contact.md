---
title: Contact
subtitle: Please use this form to get in touch.
comments: false
---

<div id="contact-form-container" class="container">
    <div class="col-lg-8 well">
        <form id="contact-form">
            <div class="controls">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input id="name" type="text" name="name" class="form-control" placeholder="Please enter your name">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email *</label>
                            <input id="email" type="email" name="surname" class="form-control" placeholder="Please enter your email *" required="required">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="message">Message *</label>
                            <textarea id="message" name="message" class="form-control" placeholder="Please enter your message." rows="12" required="required"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button onclick="return postMessage();" class="btn btn-primary btn-send cc-fade-2" style="padding:0em 3em;" value="Send message"><h4>Send</h4></button>
                    </div>
                </div>
                <input id="submit_handle" type="submit" style="display: none">
        </form>
    </div>
</div>
<div id="contact-form-container" class="col-lg-8 text-center">
    <div class="row">Alternatively, you can connect with me via any of the services below...</div>
</div>

<script>
    function postMessage() {
        var name = document.getElementById('name');
        var email = document.getElementById('email');
        var message = document.getElementById('message');

        if(!name.checkValidity() || !email.checkValidity() || !message.checkValidity()) {
            document.getElementById('submit_handle').click();
        } else {
            var request = new XMLHttpRequest();

            request.addEventListener('load', function(e) {
                document.getElementById('contact-form-container').style.display = 'none';
                document.getElementsByClassName('page-subheading')[0].innerHTML = 'Thanks for getting in touch!';
            });

            request.open("POST", "https://hooks.slack.com/services/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");
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var payload = encodeURIComponent('{\"text\":\"NAME = ' + name.value + '\nEMAIL = ' + email.value + '\nMESSAGE = ' + message.value + '\"}');
            request.send('payload='+payload);
        }
        return false;
    }
</script>