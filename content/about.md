---
title: About
subtitle: A few fun-sized facts about this website.
comments: false
---

- It's built using the <a href="https://gohugo.io/" target="_blank">Hugo</a> static site generator.
- I used <a href="https://amazonlightsail.com/" target="_blank">Amazon Lightsail</a> and <a href="https://certbot.eff.org/about/" target="_blank">Certbot</a> to generate it's <a href="https://letsencrypt.org/" target="_blank">Lets Encrypt</a> SSL certificates.
- I created the [contact form](/contact/) using plain old javascript. It integrates with an <a href="https://api.slack.com/incoming-webhooks" target="_blank">Incoming Webhook</a> for my personal <a href="https://slack.com/" target="_blank">Slack</a> workspace. No need for an email server.
- Everytime I check in a change from my local environment, <a href="https://docs.gitlab.com/ce/ci/README.html" target="_blank">GitLab CI</a> automatically rebuilds and publishes it to <a href="https://about.gitlab.com/features/pages/" target="_blank">GitLab Pages</a>.
- The only cost is the domain name, apart from the week or so it took to setup.
- Creating this site has been fun, easy, and a great learning experience!

---