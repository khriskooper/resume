---
---
<div class="text-center">
    <h4 class="cc-subheading">Software Engineer with 8 years experience</h4>
    <p class="row">
        Highly collaborative software engineer specializing in back-end web development and test automation. Previous experience in Audio Technology, Game Design and Leading a team.
    
        Comfortable using the following technologies:
    </p>
    <span class="label label-default">Java</span>
    <span class="label label-default">Maven</span>
    <span class="label label-default">Spring</span>
    <span class="label label-default">JUnit</span>
    <span class="label label-default">Velocity</span>
    <span class="label label-default">ElasticSearch</span>
    <span class="label label-default">Tomcat</span>
    <span class="label label-default">MySQL</span>
    <span class="label label-default">XML</span>
    <span class="label label-default">HTML</span>
    <span class="label label-default">Xpath</span>
    <span class="label label-default">jQuery</span>
    <span class="label label-default">Git</span>
    <span class="label label-default">Linux</span>
    <span class="label label-default">Windows</span>
    <span class="label label-default">SonarQube</span>
    <span class="label label-default">Jenkins</span>
    <span class="label label-default">Selenium</span>
    <span class="label label-default">Cucumber</span>
</div>

---

<div id="downloadResumeButton" class="text-center">
    <a href="/chris-cooper-resume.pdf" download="chris-cooper-resume.pdf">
        <button type="button" class="btn btn-primary cc-fade-2" style="padding:1em 3em;" onclick="return downloadResume();"><h4><strong>Download PDF</strong></h4></button>
    </a>
</div>

<script>
    function downloadResume() {
        document.getElementById("downloadResumeButton").innerHTML='<h3 style="margin-top:37px;"><span class="label label-success" style="padding:1em 3em;">Thank You!</span></h3>';
        ga('send', 'event', 'PDF', 'Download', 'Resume – PDF Download');

        var request = new XMLHttpRequest();
        request.open("POST", "https://hooks.slack.com/services/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");
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var payload = encodeURIComponent('{\"text\":\"Another resume download! (<https://analytics.google.com/analytics/web/#realtime/rt-location/a47557374w159527816p160821438/%3F_.advseg%3D%26_.useg%3D%26_.sectionId%3D%26_r.dsa%3D1%26metric.type%3D5%26mapMode.type%3DgeoChart%26filter.list%3D42%3D%3DResume%252520%2525E2%252580%252593%252520PDF%252520Download%3B/|see in Google Analytics>)\", \"channel\":\"resume_downloads\"}');
        request.send('payload='+payload);
    }
</script>